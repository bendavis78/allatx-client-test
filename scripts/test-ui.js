var $ = document.querySelector.bind(document);
/* globals ace, client */
/* exported updateProfileView, fireEvent, defaultCallback */

var editor = ace.edit('params');
editor.$blockScrolling = Infinity;
editor.getSession().setMode('ace/mode/json');
editor.setValue(localStorage.getItem('clientParams') || '{\n  "body": {}\n}', -1);
editor.on('input', function() {localStorage.setItem('clientParams', editor.getValue());});
editor.getSession().setTabSize(2);
editor.getSession().setUseSoftTabs(true);
editor.setAutoScrollEditorIntoView(true);
editor.setOptions({minLines: 3, maxLines: Infinity});
if (localStorage.getItem('ace-vim-mode')) {
  editor.setKeyboardHandler('ace/keyboard/vim');
}

var $body = $('body');
var $profile = $('#profile');
var $img = $('#profile img');
var $name = $('#profile .name');

var clientAction = localStorage.getItem('clientAction');

function authzStatusChanged(s) {
  console.log('status changed', s);
  $('body').classList[s === 'authorized' ? 'add' : 'remove']('authorized');
  $('body').classList[s ? 'add' : 'remove']('resolved');
}

function initUi(authorization) {
  console.log('initUi');
  authzStatusChanged(authorization.status);
  authorization.on('status-changed', function(event) {
    authzStatusChanged(event.detail.status);
  });

  for (var api in client.apis) {
    if (api === 'help') {
      continue;
    }
    for (var operationName in client.apis[api].operations) {
      var operation = api + '.' + operationName;
      var option = document.createElement('option');
      option.setAttribute('value', operation);
      option.textContent = operation;
      if (operation === clientAction) {
        option.setAttribute('selected', 'selected');
      }
      $('#action').appendChild(option);
    }
  }
}

$('#action').addEventListener('change', function() {
  localStorage.setItem('clientAction', $('#action').value);
});
      
/* exported updateLoginUi */
/**
 * @param {string} provider the provider string
 * @param {string} name the user's display name
 * @param {string} image the user's profile image
 */
function updateLoginUi(provider, name, image) {
  /* jshint camelcase:false */
  if (provider) {
    $profile.dataset.provider = provider;
  } else {
    $body.classList.remove('authenticated');
    $profile.dataset.provider = '';
  }
  $name.textContent = name || '';
  $img.src = image || '';
}

function handleResponse(response) {
  $('#output').classList.remove('error');
  $('#output').textContent = JSON.stringify(response.obj, null, 2);
}

function handleError(error) {
  if (!error) {
    return;
  }
  $('#output').classList.add('error');
  if (error.data) {
    var data = JSON.parse(error.data);
    if (data && data.message) {
      $('#output').textContent = data.message;
    }
  }
  if (typeof error === 'string') {
    $('#output').textContent = error;
    throw error;
  } else if (error instanceof Error) {
    $('#output').textContent = error.name + ': ' + error.message;
    throw error;
  } else if (error.obj) {
    console.error(error.obj.code + ': ' + error.obj.message);
    $('#output').textContent = error.obj.code + ': ' + error.obj.messages;
    console.log(error.obj);
  } else if (error.data) {
    error = JSON.parse(error.data);
    $('#output').textContent = error.code + ': ' + error.message;
    console.error(error.code + ': ' + error.message);
    console.log(error.data);
  } else {
    console.error('Error: ', error);
    $('#output').textContent = error.toString();
  }
}

function defaultCallback(error, response) {
  if (error) {
    return handleError(error);
  }
  return handleResponse(response);
}

$('#go').addEventListener('click', function() {
  var action = $('#action').value.split('.');
  var params = JSON.parse(editor.getValue());
  client[action[0]][action[1]](params, handleResponse, handleError);
});


window.initUi = initUi;
window.handleError = handleError;
