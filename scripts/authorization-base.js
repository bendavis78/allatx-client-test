(function() {
/**
 * @typedef {Object} AuthzParams
 * @param {string} params.identityId
 * @param {string} params.identityPoolId
 * @param {string} params.roleArn
 * @param {string} params.provider
 * @param {string} params.token
 */

/**
 * @callback {nodeCallback}
 * @param {string} error
 * @param {Object} result
 */
/* global EventEmitter */

/**
 * Base Authorization class
 */
function Authorization() {
  this.status = null;
  try {
    this.storage = window.localStorage !== null && typeof window.localStorage === 'object' ? window.localStorage : {};
  } catch (error) {
    this.storage = {};
  }
}
Authorization.prototype = Object.create(EventEmitter.prototype);
Authorization.constructor = Authorization;


Authorization.prototype._setStatus = function(status) {
  var oldStatus = this.status;
  this.status = status;
  if (oldStatus !== status) {
    var event = {canceled: false, detail: {oldStatus: oldStatus, status: status}};
    this.emit('status-changed', event);
    if (event.canceled) {
      return;
    }
    if (status === 'authorized') {
      this.emit('authorized');
    }
  }
};


/**
 * Get current params
 */
Authorization.prototype.getParams = function() {
  if (!this._params && this.storage.params) {
    try {
      this._params = JSON.parse(this.storage.params);
    } catch(error) {
      this._params = null;
      delete this.storage.params;
      this.emit('error', error);
    }
  }
  return this._params;
};

/**
 * Set current params
 *
 * @param {AuthzParams} params
 */
Authorization.prototype.setParams = function(params) {
  try {
    this.storage.params = JSON.stringify(params);
    this._params = params;
  } catch (error) {
    this.emit('error', error);
    return;
  }
};


/**
 * Refresh authorization from the server
 *
 * @param {AuthzParams} params
 * @param {nodeCallback} callback
 */
Authorization.prototype.refresh = function(params, callback) {
  console.log('authz refresh');
  this._setStatus(null);
  this.clear();
  this.setParams(params);
  this.update(callback);
};

/**
 * Clear all credentials
 */
Authorization.prototype.clear = function() {
  console.log('authz clear');
  if (this.storage.params) {
    delete this.storage.params;
  }
  this._setStatus('unauthorized');
};

/**
 * @param {nodeCallback} callback
 */
Authorization.prototype.update = function(callback) {
  callback(null);
};

/**
 * Applies authorization headers to a request
 *
 * @param {Object} request
 */
Authorization.prototype.apply = function(request) {
  request = request;
  return true;
};

window.Authorization = Authorization;

})();
