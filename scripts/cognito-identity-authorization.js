/**
  * AWS authorization provider for swagger-js
  */
(function() {
/* global AWS, CryptoJS, Authorization */

var AWS_SHA_256 = 'AWS4-HMAC-SHA256';
var AWS4_REQUEST = 'aws4_request';
var AWS4 = 'AWS4';
var X_AMZ_DATE = 'X-Amz-Date';
var X_AMZ_SECURITY_TOKEN = 'X-Amz-Security-Token';
var HOST = 'host';
var AUTHORIZATION = 'Authorization';
var ACCEPT = 'Accept';
var CONTENT_TYPE = 'Content-Type';

function assertDefined(object, name) {
  if (object === undefined) {
   throw name + ' must be defined';
  } else {
   return object;
  }
}

function copy(obj) {
 if (null === obj || 'object' !== typeof obj) {
  return obj;
 }
 var copyObj = obj.constructor();
 for (var attr in obj) {
  if (obj.hasOwnProperty(attr)) {
    copyObj[attr] = obj[attr];
  }
 }
 return copyObj;
}

function formatDate(date, time) {
  var parts = date.toISOString().split('T');
  var amzDate = parts[0].replace(/-/g, '');
  if (!time) {
   return amzDate;
  }
  var amzTime = parts[1].replace(/:|\.\d+/g, '');
  return amzDate + 'T' + amzTime;
}

function hash(value) {
  return CryptoJS.SHA256(value);
}

function hexEncode(value) {
  return value.toString(CryptoJS.enc.Hex);
}

function hmac(secret, value) {
  return CryptoJS.HmacSHA256(value, secret, {asBytes: true});
}

function parseQueryString(queryString) {
  var pairs = queryString.slice(1).split('&');
  var result = {};
  for (var pair, i=0; i<pairs.length; i++) {
   pair = pairs[i].split('=');
   result[pair[0]] = decodeURIComponent(pair[1] || '');
  }
  return JSON.parse(JSON.stringify(result));
}
function buildCanonicalUri(uri) {
  console.log('buildCanonicalUri', uri);
  return encodeURI(uri);
}

function buildCanonicalQueryString(queryParams) {
  console.log('buildCanonicalQueryString', queryParams);
  if (Object.keys(queryParams).length < 1) {
    return '';
  }

  var sortedQueryParams = [];
  for (var property in queryParams) {
    if (queryParams.hasOwnProperty(property)) {
      sortedQueryParams.push(property);
    }
  }
  sortedQueryParams.sort();

  var canonicalQueryString = '';
  for (var i = 0; i < sortedQueryParams.length; i++) {
      canonicalQueryString += sortedQueryParams[i] + '=' + encodeURIComponent(queryParams[sortedQueryParams[i]]) + '&';
  }
  return canonicalQueryString.substr(0, canonicalQueryString.length - 1);
}

function buildCanonicalHeaders(headers) {
  console.log('buildCanonicalHeaders', headers);
    var canonicalHeaders = '';
    var sortedKeys = [];
    for (var property in headers) {
      if (headers.hasOwnProperty(property)) {
        sortedKeys.push(property);
      }
    }
    sortedKeys.sort();

    for (var i = 0; i < sortedKeys.length; i++) {
      canonicalHeaders += sortedKeys[i].toLowerCase() + ':' + headers[sortedKeys[i]] + '\n';
    }
    return canonicalHeaders;
}

function buildCanonicalSignedHeaders(headers) {
  console.log('buildCanonicalSignedHeaders', headers);
  var sortedKeys = [];
  for (var property in headers) {
    if (headers.hasOwnProperty(property)) {
      sortedKeys.push(property.toLowerCase());
    }
  }
  sortedKeys.sort();

  return sortedKeys.join(';');
}

function buildCanonicalRequest(method, path, queryParams, headers, payload) {
  console.log('buildCanonicalRequest', arguments);
  return method + '\n' +
    buildCanonicalUri(path) + '\n' +
    buildCanonicalQueryString(queryParams) + '\n' +
    buildCanonicalHeaders(headers) + '\n' +
    buildCanonicalSignedHeaders(headers) + '\n' +
    hexEncode(hash(payload));
}

function hashCanonicalRequest(request) {
  console.log('hashCanonicalRequest', request);
  return hexEncode(hash(request));
}


function buildStringToSign(amzDateTime, credentialScope, hashedCanonicalRequest) {
  console.log('buildStringToSign', arguments);
  return AWS_SHA_256 + '\n' +
    amzDateTime + '\n' +
    credentialScope + '\n' +
    hashedCanonicalRequest;
}

function buildCredentialScope(amzDate, region, service) {
  console.log('buildCredentialScope', arguments);
  return amzDate + '/' + region + '/' + service + '/' + AWS4_REQUEST;
}

function calculateSigningKey(secretKey, amzDate, region, service) {
  console.log('calculateSigningKey', arguments);
  return hmac(hmac(hmac(hmac(AWS4 + secretKey, amzDate), region), service), AWS4_REQUEST);
}

function calculateSignature(key, stringToSign) {
  console.log('calculateSignature', arguments);
  return hexEncode(hmac(key, stringToSign));
}

function buildAuthorizationHeader(accessKey, credentialScope, headers, signature) {
  console.log('buildAuthorizationHeader', arguments);
  return AWS_SHA_256 + ' Credential=' + accessKey + '/' + credentialScope + ', SignedHeaders=' + buildCanonicalSignedHeaders(headers) + ', Signature=' + signature;
}


var CognitoIdentityAuthorization = function() {
  Authorization.apply(this, arguments);
  this.defaultContentType = 'application/json';
  this.credentials = new AWS.CognitoIdentityCredentials({});
  this.defaultAcceptType = this.defaultContentType;
};
CognitoIdentityAuthorization.prototype = Object.create(Authorization.prototype);
CognitoIdentityAuthorization.constructor = CognitoIdentityAuthorization;

/**
 * @param {AuthzParams} params
 * @param {nodeCallback} callback
 */
CognitoIdentityAuthorization.prototype.update = function(callback) {
  var params = this.getParams();
  console.log('authz update', params);

  this._setStatus(null);

  if (!params || !Object.keys(params).length) {
    this._setStatus('unauthorized');
    return;
  }

  this.credentials.params = {
    IdentityPoolId: params.identityPoolId
  };

  this.credentials.params.Logins = {};
  this.credentials.params.Logins[params.provider] = params.token;
  
  if (params.identityId) {
    this.credentials.params.IdentityId = params.identityId;
  }
  this.credentials.loadCachedId();

  if (this.credentials.expired) {
    this.credentials.get(function(err) {
      if (err) {
        this._setStatus('unauthorized');
        this.emit('error', err);
        return callback && callback(err);
      }
      this._setStatus('authorized');
      return callback && callback(null, this.status);
    }.bind(this));
  } else {
    this._setStatus('authorized');
    return callback && callback(null, this.status);
  }
};

CognitoIdentityAuthorization.prototype.clear = function() {
  Authorization.prototype.clear.apply(this, arguments);
  this.credentials.clearCachedId();
};

CognitoIdentityAuthorization.prototype.apply = function(request) {
  console.log('CognitoIdentityAuthorization apply');
  this.update();

  if (this.status === 'unauthorized') {
    // don't add any auth headers
    return true;
  } else if (this.status === null) {
    // unknown state?
    return false;
  }

  var url = document.createElement('a');
  url.href = request.url;

  var verb = assertDefined(request.method, 'verb');
  var path = url.pathname;

  var queryString = request.url.split('?')[1];
  var queryParams = queryString ? parseQueryString(queryString) : {};

  var headers = copy(request.headers);
  if (headers === undefined) {
      headers = {};
  }

  //If the user has not specified an override for Content type the use default
  if (headers[CONTENT_TYPE] === undefined) {
    headers[CONTENT_TYPE] = this.defaultContentType;
  }

  //If the user has not specified an override for Accept type the use default
  if (headers[ACCEPT] === undefined) {
    headers[ACCEPT] = this.defaultAcceptType;
  }

  var body = copy(request.body);
  if (body === undefined || verb === 'GET') { // override request body and set to empty when signing GET requests
      body = '';
  }  else {
      body = JSON.stringify(body);
  }

  //If there is no body remove the content-type header so it is not included in SigV4 calculation
  if(body === '' || body === undefined || body === null) {
      delete headers[CONTENT_TYPE];
  }

  var date = new Date();
  var amzDateTime = formatDate(date, true);
  var amzDate = formatDate(date);

  headers[X_AMZ_DATE] = amzDateTime;
  headers[HOST] = url.hostname;

  var canonicalRequest = buildCanonicalRequest(verb, path, queryParams, headers, body);
  var hashedCanonicalRequest = hashCanonicalRequest(canonicalRequest);
  var credentialScope = buildCredentialScope(amzDate, this.credentials.region,
                                             this.credentials.serviceName);
  var stringToSign = buildStringToSign(amzDateTime, credentialScope, hashedCanonicalRequest);
  var signingKey = calculateSigningKey(this.credentials.secretKey, amzDate,
                                       this.credentials.region,
                                       this.credentials.serviceName);
  var signature = calculateSignature(signingKey, stringToSign);

  headers[AUTHORIZATION] = buildAuthorizationHeader(this.credentials.accessKey, credentialScope,
                                                    headers, signature);

  console.log('sessionToken: ', this.credentials.sessionToken);
  if(this.credentials.sessionToken !== undefined && this.credentials.sessionToken !== '') {
      headers[X_AMZ_SECURITY_TOKEN] = this.credentials.sessionToken;
  }
  delete headers[HOST];

  //Need to re-attach Content-Type if it is not specified at this point
  if(headers[CONTENT_TYPE] === undefined) {
      headers[CONTENT_TYPE] = this.defaultContentType;
  }

  for (var header in headers) {
    request.headers[header] = headers[header];
  }

  console.debug('request headers: ', headers);
  return true;
};

window.CognitoIdentityAuthorization = CognitoIdentityAuthorization;

})();
