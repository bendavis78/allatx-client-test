(function() {
/* global Authorization */

var X_DEVSERVER_AUTHORIZATION = 'X-Devserver-Authorization';

function DevserverAuthorization() {
  Authorization.apply(this, arguments);
}
DevserverAuthorization.prototype = Object.create(Authorization.prototype);
DevserverAuthorization.constructor = DevserverAuthorization;

DevserverAuthorization.prototype.update = function(callback) {
  console.log('update');
  var params = this.getParams();
  this._setStatus(params && params.identityId ? 'authorized' : 'unauthorized');
  return callback && callback(null, this.status);
};

DevserverAuthorization.prototype.apply = function(request) {
  console.log('apply');
  this.update();
  var params = this.getParams();
  if (params && params.identityId) {
    request.headers[X_DEVSERVER_AUTHORIZATION] = params.identityId;
  }
  return true;
};

window.DevserverAuthorization = DevserverAuthorization;

})();
